<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("configuracion");
	}

  public function index(){
	$data["configuracion"]=$this->configuracion->obtenerTodos();
    $this->load->view("header");
    $this->load->view("configuraciones/index",$data);
    $this->load->view("footer");
  }
  //insercion Asincrona
  public function guardar(){
    $datosCon=array(
      "nombre_con_bqt"=>$this->input->post("nombre_con_bqt"),
      "ruc_con_bqt"=>$this->input->post("ruc_con_bqt"),
      "telefono_con_bqt"=>$this->input->post("telefono_con_bqt"),
      "direccion_con_bqt"=>$this->input->post("direccion_con_bqt"),
      "representante_con_bqt"=>$this->input->post("representante_con_bqt")
    );
    if($this->configuracion->insertar($datosCon)){
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Configuracion insertado exitosamente"
      );
    }else{
      $resultado=array(
        "estado"=>"error"
      );
    }
    echo json_encode($resultado);
  }
	//funcion para consultar profesores en formato JSON
	public function listado(){
			$data["configuracion"]=$this->configuracion->obtenerTodos();
			//$this->load->view("header");
			$this->load->view("configuraciones/listado",$data);
			//$this->load->view("footer");
	}
	
    public function borrar($id_con_bqt){
		if ($this->configuracion->eliminarPorId($id_con_bqt)){
			$this->session
			->set_flashdata('confirmacion',
		 'Estudiante ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('configuraciones/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["Editar"]=
			$this->configuracion->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("configuraciones/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosEditado=array(
			"nombre_con_bqt"=>$this->input->post("nombre_con_bqt"),
            "ruc_con_bqt"=>$this->input->post("ruc_con_bqt"),
            "telefono_con_bqt"=>$this->input->post("telefono_con_bqt"),
            "direccion_con_bqt"=>$this->input->post("direccion_con_bqt"),
            "representante_con_bqt"=>$this->input->post("representante_con_bqt")
		);
	    $id=$this->input->post("id_con_bqt");
		if($this->configuracion->actualizar($id,$datosEditado)){
			redirect('configuraciones/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}//cierre de la clase









//
