<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitudes extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("solicitud");
	}

  public function index(){
	$data["solicitudes"]=$this->solicitud->obtenerTodos();
    $this->load->view("header");
    $this->load->view("solicitudes/index",$data);
    $this->load->view("footer");
  }
  //insercion Asincrona
  public function guardar(){
    $datosSolicitudes=array(
      "motivo_sol_bqt"=>$this->input->post("motivo_sol_bqt"),
      "descripcion_sol_bqt"=>$this->input->post("descripcion_sol_bqt"),
      "fecha_sol_bqt"=>$this->input->post("fecha_sol_bqt")

    );
    if($this->solicitud->insertar($datosSolicitudes)){
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Configuracion insertado exitosamente"
      );
    }else{
      $resultado=array(
        "estado"=>"error"
      );
    }
    echo json_encode($resultado);
  }
	//funcion para consultar  en formato JSON
	public function listado(){
			$data["solicitudes"]=$this->solicitud->obtenerTodos();
			//$this->load->view("header");
			$this->load->view("solicitudes/listado",$data);
			//$this->load->view("footer");
	}
	
    public function borrar($id_sol_bqt){
		if ($this->solicitud->eliminarPorId($id_sol_bqt)){
			$this->session
			->set_flashdata('confirmacion',
		 'Estudiante ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('solicitudes/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["editarSolicitud"]=
			$this->solicitud->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("solicitudes/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosEditado=array(
			"motivo_sol_bqt"=>$this->input->post("motivo_sol_bqt"),
      		"descripcion_sol_bqt"=>$this->input->post("descripcion_sol_bqt"),
      		"fecha_sol_bqt"=>$this->input->post("fecha_sol_bqt")
		);
	    $id=$this->input->post("id_sol_bqt");
		if($this->solicitud->actualizar($id,$datosEditado)){
			redirect('solicitudes/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}//cierre de la clase









//
