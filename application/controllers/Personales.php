<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personales extends CI_Controller {
	//Definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model('personal');
	}
	//renderiza vista personal
	public function index()
	{
		$data['listadoPersonales']=$this->personal->obtenerTodos();
		$this->load->view('header.php');
		$this->load->view('personales/index',$data);
		$this->load->view('footer.php');
	}

	//Funcion para capturar los valores del formulario Nuevo
	public function guardarPersonales(){
		$datosNuevoPersonales=array(
			'nombre_per_bqt'=>$this->input->post('nombre_per_bqt'),
			'apellido_per_bqt'=>$this->input->post('apellido_per_bqt'),
			'cedula_per_bqt'=>$this->input->post('cedula_per_bqt'),
			'telefono_per_bqt'=>$this->input->post('telefono_per_bqt'),
      'fechana_per_bqt'=>$this->input->post('fechana_per_bqt'),
      'cargo_per_bqt'=>$this->input->post('cargo_per_bqt'),
      'direccion_per_bqt'=>$this->input->post('direccion_per_bqt'),
      'email_per_bqt'=>$this->input->post('email_per_bqt'),

		);
		// print_r($datosNuevoPersonales);
		if ($this->personal->insertar($datosNuevoPersonales)) {
			
			$resultado=array("estado"=>"ok","mensaje"=>"Personal insertado exitosamente");
		}else {
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

	public function borrar($id_per_bqt){
		
		if ($this->personal->eliminarPorId($id_per_bqt)) {
			redirect('personales/index');
			enviarEmail($datosNuevoPersonales["usuario"],
			"NOTIFICAION DE REGISTRO",
			"EL PROFERSOR".$datosNuevoPersonales["usuario"]."FUE ELIMINADO EL DIA".date("Y-m-d H:i:s")
		);
		} else {
			echo "ERROR AL ELIMINAR :)";
		}
	}


}//Cierre de la clase (No borrar)
