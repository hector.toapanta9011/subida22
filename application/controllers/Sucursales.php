<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursales extends CI_Controller {
	//Definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model('sucursal');
	}
	//renderiza vista estudiantes
	public function index()
	{
		$this->load->view('header.php');
		$this->load->view('sucursales/index');
		$this->load->view('footer.php');
	}
	//Funcion para capturar los valores del formulario Nuevo
	public function guardarSucursales(){
		$datosNuevoSucursales=array(
			'provincia_suc_bqt'=>$this->input->post('provincia_suc_bqt'),
			'ciudad_suc_bqt'=>$this->input->post('ciudad_suc_bqt'),
			'estado_suc_bqt'=>$this->input->post('estado_suc_bqt'),
      		'direccion_suc_bqt'=>$this->input->post('direccion_suc_bqt'),
      		'email_suc_bqt'=>$this->input->post('email_suc_bqt')
		);
		if ($this->sucursal->insertar($datosNuevoSucursales)) {
			$resultado=array("estado"=>"ok","mensaje"=>"Sucursal insertado exitosamente");
		}else {
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}
	//Funcion para consultar sucursal  en formato JSON
	public function listado(){
		$data['sucursales']=$this->sucursal->obtenerTodos();
    // $data['sucursalEditar']=$this->sucursal->obtenerPorId($id_suc_bqt);
		$this->load->view('sucursales/listado',$data);
	}
  public function eliminarSucursal($id_suc_bqt){
		$id_suc_bqt=$this->input->post("id_suc_bqt");
		if ($this->sucursal->eliminarPorId($id_suc_bqt)) {
			echo json_encode(array("respuesta"=>"ok"));
		} else {
			echo json_encode(array("respuesta"=>"error"));
		}

	}
	
  public function editar($id_suc_bqt){
     $data["sucursalEditar"]=$this->sucursal->obtenerPorId($id_suc_bqt);
     $this->load->view("sucursales/editar",$data);
   }

   public function actualizarSucursal(){
       $id_suc_bqt=$this->input->post("id_suc_bqt");
       $data=array(
		'provincia_suc_bqt'=>$this->input->post('provincia_suc_bqt'),
		'ciudad_suc_bqt'=>$this->input->post('ciudad_suc_bqt'),
		'estado_suc_bqt'=>$this->input->post('estado_suc_bqt'),
		'direccion_suc_bqt'=>$this->input->post('direccion_suc_bqt'),
		'email_suc_bqt'=>$this->input->post('email_suc_bqt')
       );
       if($this->sucursal->actualizar($data,$id_suc_bqt)){
           echo json_encode(array("respuesta"=>"ok"));
       }else{
           $resultado=array("estado"=>"error");
       }
			 echo json_encode($resultado);
   }

}//Cierre de la clase (No borrar)
