<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Capacitaciones extends CI_Controller {
	//Definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model('capacitacion');
		$this->load->model('departamento');
	}
  //redirecion de la duncion index
  public function index(){
		$data['listadoDepartamentos']=$this->departamento->obtenerTodos();
		$data['listadoCapacitaciones']=$this->capacitacion->obtenerTodos();
    $this->load->view('header.php');
    $this->load->view('capacitaciones/index',$data);
    $this->load->view('footer.php');
  }
  //guardar datos de capacitaciones
  public function guardarCapacitaciones(){
    $datosNuevoCapacitaciones=array(
      'tema_cap_bqt'=>$this->input->post('tema_cap_bqt'),
      'inicio_cap_bqt'=>$this->input->post('inicio_cap_bqt'),
      'fin_cap_bqt'=>$this->input->post('fin_cap_bqt'),
			'observaciones_cap_bqt'=>$this->input->post('observaciones_cap_bqt'),
			'fk_id_dep_bqt'=>$this->input->post('fk_id_dep_bqt')
    );
    if ($this->capacitacion->insertar($datosNuevoCapacitaciones)) {
      $resultado=array('estado'=>'ok');
    } else {
      $resultado=array('estado'=>'error');
    }
    echo json_encode($resultado);
  }
	

	public function borrar($id_cap_bqt){
		if ($this->capacitacion->eliminarPorId($id_cap_bqt)) {
			redirect('capacitaciones/index');
		} else {
			echo "ERROR AL ELIMINAR :)";
		}
	}
}//Cierre de la clase (No borrar)
