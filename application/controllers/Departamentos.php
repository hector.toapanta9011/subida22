<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamentos extends CI_Controller {
	//Definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model('departamento');
	}
  //redirecion de la duncion index
  public function index(){
		$data['listadoDepartamentos']=$this->departamento->obtenerTodos();
    $this->load->view('header.php');
    $this->load->view('departamentos/index',$data);
    $this->load->view('footer.php');
  }
  //guardar datos de departamentos
  public function guardarDepartamentos(){
    $datosNuevoDepartamentos=array(
      'nombre_dep_bqt'=>$this->input->post('nombre_dep_bqt'),
      'email_dep_bqt'=>$this->input->post('email_dep_bqt'),
      'telefono_dep_bqt'=>$this->input->post('telefono_dep_bqt')
    );
    if ($this->departamento->insertar($datosNuevoDepartamentos)) {
      $resultado=array('estado'=>'ok');
    } else {
      $resultado=array('estado'=>'error');
    }
    echo json_encode($resultado);
  }

	public function borrar($id_dep_bqt){
		if ($this->departamento->eliminarPorId($id_dep_bqt)) {
			redirect('departamentos/index');
		} else {
			echo "ERROR AL ELIMINAR :)";
		}
	}
}//Cierre de la clase (No borrar)
