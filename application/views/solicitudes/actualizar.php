<div class="row">
  <div class="col-md-12 text-center well">
      <h3>ACTUALIZAR SOLICITUD</h3>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-12">
    <?php if ($editarSolicitud): ?>
        <!-- <?php print_r($editarSolicitud); ?> -->
        <form id="frm_nuevo_solicitud" class="container-fluid" action="<?php echo site_url('Solicitudes/procesarActualizacion'); ?>" method="post">
            <center>
              <input type="hidden" name="id_sol_bqt" value="<?php echo $editarSolicitud->id_sol_bqt; ?>">
            </center>
            <b>Motivo Solicitud:</b><br>
            <input type="text" id="motivo_sol_bqt" name="motivo_sol_bqt" value="<?php echo $editarSolicitud->motivo_sol_bqt; ?>" placeholder="Ingrese motivo" class="form-control"> <br>
            <br>
            <b>Descripcion:</b><br>
            <input type="text" id="descripcion_sol_bqt" name="descripcion_sol_bqt" value="<?php echo $editarSolicitud->descripcion_sol_bqt; ?>" placeholder="Ingrese Descripcion" class="form-control"> <br>
            <br>
            <b>Fecha Solicitud:</b><br>
            <input type="date" id="fecha_sol_bqt" name="fecha_sol_bqt" value="<?php echo $editarSolicitud->fecha_sol_bqt; ?>" class="form-control"> <br>
            <br>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i>Guardar</button>
            <a href="<?php echo site_url('configuraciones/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Cancelar</a>
        </form>

    <?php else: ?>
        <div class="alert alert-danger">
            <b>No se encontro configuraciones:(</b>
        </div>
    <?php endif; ?>
  </div>
</div>
