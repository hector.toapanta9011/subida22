<?php if($solicitudes):?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>MOTIVO</th>
                <th>DESCRIPCION</th>
                <th>FECHA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($solicitudes->result() as $temporal): ?>
                <tr>
                    <td class="text-center"><?php echo $temporal->id_sol_bqt; ?></td>
                    <td class="text-center"><?php echo $temporal->motivo_sol_bqt; ?></td>
                    <td class="text-center"><?php echo $temporal->descripcion_sol_bqt; ?></td>
                    <td class="text-center"><?php echo $temporal->fecha_sol_bqt; ?></td>
                    <td class="text-center">
                        <a href="<?php echo site_url("solicitudes/actualizar") ?>/<?php echo $temporal->id_sol_bqt; ?>" class="fa fa-edit"><i><i></a>
                        <a href="<?php echo site_url("solicitudes/borrar") ?>/<?php echo $temporal->id_sol_bqt; ?>" class="fa fa-trash"><i<i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else:?>
    <div class="alert alert-danger">
        <center>
            NO EXISTEN SOLICITUDES
        </center>
    </div>
<?php endif;?>