<center>
  <h1> <b>SOLICITUDES</b> </h1>
</center>

<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <center>
        <h1></h1>
      </center>
      <center>
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info" 
        data-bs-toggle="modal" data-bs-target="#modalSolicitud">AGREGAR</button>
      </center>
    </div>
</div>
<!-- Modal -->
<div id="modalSolicitud" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>INGRESE SOlLICITUD</b></h4>
      </div>
      <div class="modal-body">
          <form id="frm_nuevo_solicitud" class="" action="<?php echo site_url('Solicitudes/guardar'); ?>" method="post">
            <b>Motivo Solicitud:</b><br>
            <input type="text" id="motivo_sol_bqt" name="motivo_sol_bqt" value="" placeholder="Ingrese motivo" class="form-control"> <br>
            <br>
            <b>Descripcion:</b><br>
            <input type="text" id="descripcion_sol_bqt" name="descripcion_sol_bqt" value="" placeholder="Ingrese Descripcion" class="form-control"> <br>
            <br>
            <b>Fecha Solicitud:</b><br>
            <input type="date" id="fecha_sol_bqt" name="fecha_sol_bqt" value="" class="form-control"> <br>
            <br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-ok"></i> Guardar
            </button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
        </div>
      </div>
  </div>
</div>


<div class="container">
  <div id="contenedor-listado-conf">
    <center>
      <i class="fa fa-spinner fa-6x fa-spin"></i>
      <br>Espere por favor...
    </center>
  </div>
</div>
<script type="text/javascript">
    function consultarConf(){
      $("#contenedor-listado-conf")
      .html('<center><i class="fa fa-spinner fa-6x fa-spin"></i><br>Espere por favor...</center>');
      $("#contenedor-listado-conf")
      .load("<?php echo site_url('solicitudes/listado'); ?>");
    }
    consultarConf();//cargando datos de forma automatic
</script>

<script>
  $("#frm_nuevo_solicitud").validate({
    rules:{
      motivo_sol_bqt:{
        required:true
      },
      descripcion_sol_bqt:{
        required:true
      },
      fecha_sol_bqt:{
        required:true,
        date:true
      }
    },
    messages:{
      motivo_sol_bqt:{
        required:"Ingrese motivo"
      },
      descripcion_sol_bqt:{
        required:"Ingrese descripcion"
      },
      fecha_sol_bqt:{
        required:"Escoja fecha",
        date:"Fecha incorrecta"
      }
    },
    submitHandler:function(formulario){
          //Ejecutando la peticion Asincrona
          $.ajax({
            type:'post',
            url:'<?php echo site_url("solicitudes/guardar"); ?>',
            data:$(formulario).serialize(),
            success:function(data){
              // alert(data);
              var objetoRespuesta=JSON.parse(data);
              if(objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK"){
                Swal.fire(
                  'CONFIRMACIÓN', //titulo
                  objetoRespuesta.mensaje,
                  'success' //Tipo de alerta
                );
                $("#modalSolicitud").modal("hide");
                consultarConf();
              }else{
                Swal.fire(
                  'ERROR', //titulo
                  'Error al insertar, intente nuevamente',
                  'error' //Tipo de alerta
                );
              }
            }
          });
      }
  });
</script>