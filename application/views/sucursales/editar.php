
<form class="" action="<?php echo site_url('sucursales/actualizar'); ?>" method="post" id="frm_actualizar_sucursal">
    <input type="hidden" name="id_usu" id="id_usu" value="<?php echo $sucursalEditar->id_suc_bqt; ?>">
    <b>PPROVINCIA:</b>
    <br>
    <input type="text" id="provincia_suc_bqt" name="provincia_suc_bqt" value="<?php echo $sucursalEditar->provincia_suc_bqt; ?>" placeholder="Ingrese la provincia" class="form-control">
    <br>
    <b>CIUDAD:</b>
    <br>
    <input type="text" id="ciudad_suc_bqt" name="ciudad_suc_bqt" value="<?php echo $sucursalEditar->ciudad_suc_bqt; ?>" placeholder="Ingrese la ciudad" class="form-control">
    <br>
    <b>ESTADO:</b>
    <br>
    <select class="form-control" name="estado_suc_bqt" id="estado_suc_bqt1">
        <option value= " ">--Seleccione una opcion</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>
    <script type="text/javascript">
        $("#estado_suc_bqt1").val("<?php echo $sucursalEditar->estado_suc_bqt; ?>");
    </script>
    <br>
    <b>DIRECCIÓN:</b>
    <br>
    <input type="text" id="direccion_suc_bqt" name="direccion_suc_bqt" value="<?php echo $sucursalEditar->direccion_suc_bqt; ?>" placeholder="Ingrese la direeccion" class="form-control">
    <br>
    <b>E-MAIL:</b>
    <br>
    <input type="email" id="email_suc_bqt" name="email_suc_bqt" value="<?php echo $sucursalEditar->email_suc_bqt; ?>" class="form-control" placeholder="Ingrese el e-mail">
    <br>

    <button type="button" onclick="actualizar()" name="button" class="btn btn-success"> <i class="fa fa-pen"></i> Actualizar </button>
</form>

<script type="text/javascript">
function actualizar(){
    $.ajax({
        url:$("#frm_actualizar_sucursal").prop("action"),
        data:$("#frm_actualizar_sucursal").serialize(),
        type:"post",
        success:function(data){
          consultarSucursales();
          $("#modalEditarSucursal").modal("hide");
          $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
          $('.modal-backdrop').remove();//eliminamos el backdrop del modal
          var objetoRespuesta=JSON.parse(data);
          if(objetoRespuesta.estado=='ok'){
            iziToast.success({
                 title: 'CONFIRMACIÓN',
                 message: 'Actualización Exitosa',
                 position: 'topRight',
               });
          }else{
            iziToast.error({
                 title: 'ERROR',
                 message: 'Error al procesar',
                 position: 'topRight',
               });
          }

        }
      });
  }
</script>












<!-- <script type="text/javascript">

function actualizar(){

    $.ajax({
      url:$("#frm_actualizar_sucursal").prop("action"),
      data:$(formulario).serialize(),
      type:"post",
      success:function(data){
        consultarSucursales();
        $("#modalEditarSucursal").modal("hide");
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        var objetoRespuesta=JSON.parse(data);
        if(objetoRespuesta.estado=='ok'){
          iziToast.success({
               title: 'CONFIRMACIÓN',
               message: 'Actualización Exitosa',
               position: 'topRight',
             });
        }else{
          iziToast.error({
               title: 'ERROR',
               message: 'Error al procesar',
               position: 'topRight',
             });
        }

      }
    });
}

</script> -->


<!-- <script type="text/javascript">
  $(document).ready(function(){
      $("#frm_actualizar_sucursal").validate({
        rules:{
          provincia_suc_bqt:{
            required:true
          },
          ciudad_suc_bqt:{
            required:true
          },
          estado_suc_bqt:{
            required:true
          },
          direccion_suc_bqt:{
            required:true
          },
          email_suc_bqt:{
            required:true
          }
        },
        messages:{
          provincia_suc_bqt:{
            required:"Por favor ingrese la provincia"
          },
          ciudad_suc_bqt:{
            required:"Por favor ingrese la ciudad"
          },
          estado_suc_bqt:{
            required:"Por favor ingrese el apellido"
          },
          direccion_suc_bqt:{
            required:"Por favor ingrese su direcciom"
          },
          email_suc_bqt:{
            required:"Por favor ingrese e-mail"
          }

        },
      submitHandler:function(form){

        var formData = new FormData($(form)[0]);

          $.ajax({
            type:'post',
            url:"<?php echo site_url('sucursales/editar'); ?>",
            data:formData,
            contentType: false,
            processData: false,
            data:$(form).serialize(),
            success:function(data){
                var objetoRespuesta=JSON.parse(data);
                if (objetoRespuesta.estado=="ok") {
                  Swal.fire('CONFIRMACION',objetoRespuesta.mensaje,'success');
                    $("#modalEditarSucursal").modal("hide");
                    cargarEdicion();
                }else {
                  Swal.fire('ERROR','Error al actualizar, intente nuevamente','error');

                }

              }

          });
          return false;
      }
    });
  });
</script> -->
