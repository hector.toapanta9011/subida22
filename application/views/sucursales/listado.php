<?php if ($sucursales): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_sucursales">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">PROVINCIA</th>
        <th class="text-center">CIUDAD</th>
        <th class="text-center">ESTADO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">E-MAIL</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($sucursales->result() as $sucursal): ?>
        <tr>
          <td class="text-center"><?php echo $sucursal->id_suc_bqt; ?></td>
          <td class="text-center"><?php echo $sucursal->provincia_suc_bqt; ?></td>
          <td class="text-center"><?php echo $sucursal->ciudad_suc_bqt; ?></td>
          <td class="text-center"><?php echo $sucursal->estado_suc_bqt; ?></td>
          <td class="text-center"><?php echo $sucursal->direccion_suc_bqt; ?></td>
          <td class="text-center"><?php echo $sucursal->email_suc_bqt; ?></td>
          <td class="text-center">
            <a  onclick="cargarEdicion(<?php echo $sucursal->id_suc_bqt; ?>);"class="btn btn-warning"><i class="fa fa-edit"></i></a>
            <a href="#" class="btn btn-danger" onclick="eliminarSucursal(<?php echo $sucursal->id_suc_bqt; ?>);"> <i class="fa fa-trash"></i></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <!-- <script type="text/javascript">
    $("#tbl_sucursales").DataTable();
  </script> -->
<?php else: ?>
  <h2><b>No existe sucursales</b></h2>
<?php endif; ?>

<!-- <script type="text/javascript">
  function eliminarSucursal(id_suc_bqt){

    iziToast.question({
    timeout: 20000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex:,
    title: '¿Seguro?',
    message: 'Desea eliminar',
    position: 'center',
    buttons: [
                ['<button><b>YES</b></button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        url:"<?php echo site_url('sucursales/eliminarSucursal'); ?>",
                        type:"post",
                        data:{"id_suc_bqt":id_suc_bqt},
                        success:function(data){
                          consultarSucursales();
                          alert(data);
                        }
                    });

                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ],
            onClosing: function(instance, toast, closedBy){
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                console.info('Closed | closedBy: ' + closedBy);
            }
        });


  }
 </script> -->
