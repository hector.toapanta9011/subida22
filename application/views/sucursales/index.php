<center>
  <h1> <b>GESTION DE SUCURSALES</b> </h1>
</center>
<!-- Trigger the modal with a button -->
<center>
  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#nuevoSucursal"> <i class="glyphicon glyphicon-plus"></i>Agregar</button>
</center>
<br>
<!-- El Modal -->
<div class="modal" id="nuevoSucursal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Formulario de registro</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <!-- <p>Some text in the modal.</p> -->
        <form id="frm_nuevo_sucursal" class="" action=" <?php echo site_url('sucursales/guardarSucursales') ?>" method="post">

          <b>PPROVINCIA:</b>
          <br>
          <input type="text" id="provincia_suc_bqt" name="provincia_suc_bqt" value="" placeholder="Ingrese la provincia" class="form-control">
          <br>
          <b>CIUDAD:</b>
          <br>
          <input type="text" id="ciudad_suc_bqt" name="ciudad_suc_bqt" value="" placeholder="Ingrese la ciudad" class="form-control">
          <br>
          <b>ESTADO:</b>
          <br>
          <select class="form-control" name="estado_suc_bqt" id="estado_suc_bqt">
              <option value= " ">--Seleccione una opcion</option>
              <option value="Activo">Activo</option>
              <option value="Inactivo">Inactivo</option>
          </select>
          <br>
          <b>DIRECCIÓN:</b>
          <br>
          <input type="text" id="direccion_suc_bqt" name="direccion_suc_bqt" value="" placeholder="Ingrese la direeccion" class="form-control">
          <br>
          <b>E-MAIL:</b>
          <br>
          <input type="email" id="email_suc_bqt" name="email_suc_bqt" value="" class="form-control" placeholder="Ingrese el e-mail">
          <br>
          <button   type="submit" name="button" class="btn btn-success">
            <i class="glyphicon glyphicon-ok" ></i>
            GUARDAR
          </button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Salir</button>
      </div>

    </div>
  </div>
</div>
<!-- LLamar a listado.php  -->
<div class="container-fluid">
  <div id="contenedor-listado-sucursales">

  </div>
</div>

<!-- Editar sucursales -->
<div class="modal" id="modalEditarSucursal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Editar Sucursales</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor_editar">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">
  function consultarSucursales(){
    $('#contenedor-listado-sucursales').html('<center> <i class="fa fa-spinner fa-6x fa-spin"> <br> Esperando por favor .....</i></center>');
    $('#contenedor-listado-sucursales').load('<?php echo site_url('sucursales/listado'); ?>');
  }
  consultarSucursales();
</script>
<script type="text/javascript">
  $("#frm_nuevo_sucursal").validate({
    rules:{
      provincia_suc_bqt:{
        letras:true,
        required:true
      },
      ciudad_suc_bqt:{
        required:true
      },
      estado_suc_bqt:{
        required:true
      },
      direccion_suc_bqt:{
        required:true
      },
      email_suc_bqt:{
        required:true
      }
    },
    messages:{
      provincia_suc_bqt:{
        letras:"Este campo solo acepta letras",
        required:"Por favor ingrese la provincia"
      },
      ciudad_suc_bqt:{
        required:"Por favor ingrese la ciudad"
      },
      estado_suc_bqt:{
        required:"Por favor ingrese el apellido"
      },
      direccion_suc_bqt:{
        required:"Por favor ingrese su direcciom"
      },
      email_suc_bqt:{
        required:"Por favor ingrese e-mail"
      }

    },
    submitHandler:function(formulario){
      //Ejecutando la peticion asincrona
      $.ajax({
        type:'post',
        url:'<?php echo site_url('sucursales/guardarSucursales'); ?>',
        data:$(formulario).serialize(),
        success:function(data){
          var objetoRespuesta=JSON.parse(data);
          if (objetoRespuesta.estado=='ok'){
            Swal.fire(
              'confirmacion!', //Titulo
              objetoRespuesta.mensaje, //Contenido o mensaje
              'success'//tipo de alerta
            );
            $('#nuevoSucursal').modal('hide');
            consultarSucursales();
          } else {
            Swal.fire(
              'error!', //Titulo
              'Error al insertar, intente nuevamente', //Contenido o mensaje
              'error'//tipo de alerta
            )
          }

        }
      });
    }

  });
</script>



<script type="text/javascript">
    function cargarEdicion(id){
      $("#contenedor_editar").html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor_editar").load("<?php echo site_url('sucursales/editar'); ?>/"+id);
      $("#modalEditarSucursal").modal("show");
    }
</script>
