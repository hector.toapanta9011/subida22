<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- importacion para dispocitivos moviles responsibilidad  -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TALENTO HUMANO</title>
    <!-- IMPORTACION JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
      <!-- FIN DE IMPORTACION  -->
      <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Importación de SweetAlert2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <?php if ($this->session->flashdata('confirmacion')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'CONFIRMACIÓN', //titulo
            '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
            'success' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>
    <?php if ($this->session->flashdata('error')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'ERROR', //titulo
            '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
            'error' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>
    </head>
    <body style="background-color: #123c5c; background-image: url(<?php echo base_url()?>/assets/images/login.png); background-position: center top; background-size: 100%; background-repeat: no-repeat;">
    <br>
    <br>
    <br>

 
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
      <h1 style="font-size:4.5vw;color:#48B1F0;; "> Bienvenido</h1>
      <b><h1 style="font-size:0.7vw;color:white;letter-spacing:0.45vw; ">SISTEMA DE TALENTO HUMANO</h1></b>
				<h1 style="font-size:3.5vw; color:#FC0F0F ;">____________</h1>
        
      <br></div></div>
      
      <div class="col-md-4"></div>
      <div class="col-md-3"style="background-image: url(<?php echo base_url()?>/assets/images/login1.png);" >
        <center>
          <h3>
             <i class="glyphicon glyphicon-lock"></i>
             <b><p style="color:white";>INICIO DE SESIÓN</p>
          </h3>
          <br>
          <p style="color:white"; >Ingresa tu nombre de usuario y contraseña</p>
        </center></b>
        <br>
        <form class="form-horizontal" id="frm_login"method="post"
        action="<?php echo site_url('seguridades/validarUsuario'); ?>">
            <fieldset>
              <!-- Text input-->
              <div class="form-group">
                <div class="col-md-12">
                <input id="email_usu_bqt" name="email_usu_bqt" type="text" placeholder="INGRESE SU EMAIL" class="form-control input-md" required="">
                </div>
              </div>

              <!-- Password input-->
              <div class="form-group">
                <div class="col-md-12">
                  <input id="password_usu_bqt" name="password_usu_bqt" type="password" placeholder="INGRESE SU PASSWORD" class="form-control input-md" required="">

                </div>
              </div>
              <br><br>
              <!-- Button (Double) -->
              <div class="form-group">
                <label class="col-md-2 control-label" for="button1id"></label>
                <div class="col-md-4">
                  <button type="submit" class="btn btn-primary"> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbspINGRESAR &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</button>
                 
                </div>
              </div>
              </fieldset>
          </form>
      </div>
      <div class="col-md-4"></div>
    </div>
    </body>   
   
    <style>

        label.error{
            border:1px solid white;
            color:white;
            background-color:#E15B69;
            padding:5px;
            padding-left:15px;
            padding-right:15px;
            font-size:12px;
            opacity: 0;
              /*visibility: hidden;*/
              /*position: absolute;*/
              left: 0px;
              transform: translate(0, 10px);
              /*background-color: white;*/
              /*padding: 1.5rem;*/
              box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
              width: auto;
              margin-top:30px !important;


              z-index: 10;
              opacity: 1;
              visibility: visible;
              transform: translate(0, -20px);
              transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
              border-radius:10px;
              width:100%;

        }
        input.error{
            border:1px solid #E15B69;
        }
        select.error{
            border:1px solid #E15B69;
        }
        label.error:before{
             position: absolute;
              z-index: -1;
              content: "";
              right: calc(90% - 10px);
              top: -8px;
              border-style: solid;
              border-width: 0 10px 10px 10px;
              border-color: transparent transparent #E15B69 transparent;
              transition-duration: 0.3s;
              transition-property: transform;
        }
    </style>
 
</html>
