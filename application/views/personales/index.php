<script type="text/javascript">
  $("#menu-personal").addClass('active');
</script>
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <center>
        <h1></h1>
      </center>
      <center>
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info" 
        data-bs-toggle="modal" data-bs-target="#modalNuevoPersonal">AGREGAR</button>
      </center>
    </div>
</div>
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <legend class="text-center">
        <i class="glyphicon glyphicon-globe"></i>
        GESTION DE TALENTO HUMANO
      </legend>
      <br>
      <br>
      <br>
      <div class="table-responsive">
        <?php if ($listadoPersonales): ?>
          <table id="tbl_personal" class="table table-striped table-bordered table-hover table-responsive">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">NOMBRE</th>
                <th class="text-center">APELLIDO</th>
                <th class="text-center">CEDULA</th>
                <th class="text-center">TELEFONO</th>
                <th class="text-center">FECHA</th>
                <th class="text-center">CARGO</th>
                <th class="text-center">DIRECCION</th>
                <th class="text-center">FECHA</th>
                <th class="text-center">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listadoPersonales->result() as $personalTemporal): ?>
                <tr>
                  <td class="text-center"><?php echo $personalTemporal->id_per_bqt;?></td>
                  <td class="text-center"><?php echo $personalTemporal->nombre_per_bqt;?></td>
                  <td class="text-center"><?php echo $personalTemporal->apellido_per_bqt;?></td>
                  <td class="text-center"><?php echo $personalTemporal->cedula_per_bqt;?></td>
                  <td class="text-center"><?php echo $personalTemporal->telefono_per_bqt;?></td>
                  <td class="text-center"><?php echo $personalTemporal->fechana_per_bqt;?></td>
                  <td class="text-center"><?php echo $personalTemporal->cargo_per_bqt; ?></td>
                  <td class="text-center"><?php echo $personalTemporal->direccion_per_bqt; ?></td>
                  <td class="text-center"><?php echo $personalTemporal->email_per_bqt; ?></td>

                  <td class="text-center">
                    <a href="<?php echo site_url('personales/borrar');?>/<?php echo $personalTemporal->id_per_bqt;?>" class="btn btn-danger" onclick="return confirm('DESEAS ELIMINAR'); ">
                    <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3><b>NO EXISTE PERSONAL</b></h3>
          <!-- letra b bols de negrita -->
        <?php endif; ?>
      </div>
    </div>
  </div>


  <!-- El Modal -->
  <div class="modal" id="modalNuevoPersonal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Formulario de registro</h4>
          <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <!-- <p>Some text in the modal.</p> -->
          <form id="frm_personal" class="" action=" <?php echo site_url('personales/guardarPersonales') ?>" method="post">

            <b>NOMBRE:</b>
            <br>
            <input type="text" id="nombre_per_bqt" name="nombre_per_bqt" value="" placeholder="Ingrese su nombre" class="form-control">
            <br>
            <b>APELLIDO:</b>
            <br>
            <input type="text" id="apellido_per_bqt" name="apellido_per_bqt" value="" placeholder="Ingrese su apellido" class="form-control">
            <br>
            <b>CEDULA:</b>
            <br>
            <input type="number" id="cedula_per_bqt" name="cedula_per_bqt" value="" placeholder="Ingrese numero cedula" class="form-control">
            <br>
            <b>TELEFONO:</b>
            <br>
            <input type="number" id="telefono_per_bqt" name="telefono_per_bqt" value="" placeholder="Ingrese numero celular" class="form-control">
            <br>
            <b>FECHA DE NACIMIENTO:</b>
            <br>
            <input type="date" id="fechana_per_bqt" name="fechana_per_bqt" value="" class="form-control">
            <br>
            <b>CARGO:</b>
            <br>
            <input type="text" id="cargo_per_bqt" name="cargo_per_bqt" value="" placeholder="Ingrese cargo Ejem. Secretaria" class="form-control">
            <br>
            <b>DIRECCIÓN:</b>
            <br>
            <input type="text" id="direccion_per_bqt" name="direccion_per_bqt" value="" placeholder="Ingrese su Direccion" class="form-control">
            <br>
            <b>EMAIL:</b>
            <br>
            <input type="text" id="email_per_bqt" name="email_per_bqt" value="" placeholder="Ingrese su Email" class="form-control">
            <br>
            <button   type="submit" name="button" class="btn btn-success">
              <i class="glyphicon glyphicon-ok" ></i>
              GUARDAR
            </button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Salir</button>
        </div>

      </div>
    </div>
  </div>



<script type="text/javascript">
$("#tbl_personal").DataTable();
</script>
<script type="text/javascript">
  $("#frm_personal").validate({
    rules:{
      nombre_per_bqt:{
        required:true,
        minlength:4
      },
      apellido_per_bqt:{
        required:true,
        minlength:4
      },
      cedula_per_bqt:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      telefono_per_bqt:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      fechana_per_bqt:{
        required:true,
        date:true
      },
      cargo_per_bqt:{
        required:true,
        minlength:4
      },
      direccion_per_bqt:{
        required:true,
        minlength:4
      },
      email_per_bqt:{
        required:true,
        email:true
      }

    },
    messages:{
        nombre_per_bqt:{
        required:"Ingrese su nombre",
        minlength:"Nombre incorrecto"
      },
      apellido_per_bqt:{
        required:"Ingrese su apellido",
        minlength:"Apellido incorrecto"
      },
      cedula_per_bqt:{
        required:"Ingrese su cedula",
        minlength:"Ingrese 10 campos",
        maxlength:"Ingrese 10 campos",
        digits:"Ingrese solo numeros"
      },
      telefono_per_bqt:{
        required:"Ingrese su telefono",
        minlength:"Ingrese 10 campos",
        maxlength:"Ingrese 10 campos",
        digits:"Ingrese solo numeros"
      },
      fechana_per_bqt:{
        required:"Ingrese fecha",
        date:"Ingrese solo numeros"
      },
      cargo_per_bqt:{
        required:"Ingrese Cargo Ejem. secretaria, ventas",
        minlength:"Cargo incorrecto"
      },
      direccion_per_bqt:{
        required:"Porfavor complete el campo",
        minlength:"Direccion incorrecta"
      },
      email_per_bqt:{
        required:"Ingrese su Email",
        email:"Email incorrecto"
      }

    },
      submitHandler:function(formulario){
        $.ajax({
          type:'post',
          url:'<?php echo site_url("personales/guardarPersonales") ?>',
          data:$(formulario).serialize(),
          success:function(data){
            var objetoRespuesta=JSON.parse(data);
            if (objetoRespuesta.estado=="ok") {
              Swal.fire(
                'confirmacion',
                objetoRespuesta.mensaje,
                'success'
              );
              $("#modalNuevoPersonal").modal("hide");
            } else {
                Swal.fire(
                  'error',
                  'error al ingresar datos',
                  'error'
              );
            }
          }
        });
      }
  });
  </script>