<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <center>
        <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#modalNuevoCapacitacion">AGREGAR</button>
      </center>
    </div>
</div>
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <legend class="text-center">
        <i class="glyphicon glyphicon-globe"></i>
        GESTION DE CAPACITACIONES
      </legend>
      <br>
      <br>
      <br>
      <div class="table-responsive">
        <?php if ($listadoCapacitaciones): ?>
          <table id="tbl_capacitacion" class="table table-striped table-bordered table-hover table-responsive">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">TEMA</th>
                <th class="text-center">INICIO CAPACITACION</th>
                <th class="text-center">FIN DE CAPACITACION</th>
                <th class="text-center">OBSERVACION</th>
                <!-- <th class="text-center">DEPARTAMENTO</th> -->
                <th class="text-center">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listadoCapacitaciones->result() as $capacitacion): ?>
                <tr>
                  <td class="text-center"><?php echo $capacitacion->id_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->tema_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->inicio_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->fin_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->observaciones_cap_bqt;?></td>
                  <!-- <td class="text-center"><?php echo $capacitacion->nombre_dep_bqt;?></td> -->

                  <td class="text-center">
                    <a href="<?php echo site_url('capacitaciones/borrar');?>/<?php echo $capacitacion->id_cap_bqt;?>" class="btn btn-danger" onclick="return confirm('DESEAS ELIMINAR'); ">
                    <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3><b>NO EXISTE CAPACITACION</b></h3>
          <!-- letra b bols de negrita -->
        <?php endif; ?>
      </div>
    </div>
  </div>

<!-- The Modal -->
<div class="modal" id="modalNuevoCapacitacion">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Formulario de nueva capacitacion</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm_capacitacion" class="" action=" <?php echo site_url('capacitaciones/guardarCapacitaciones') ?>" method="post">
          <b>TEMA:</b>
          <br>
          <input type="text" id="tema_cap_bqt" name="tema_cap_bqt" value="" placeholder="Ingrese el nombre del  capacitacion" class="form-control" required>
          <br>
          <b>INICIO:</b>
          <br>
          <input type="date" id="inicio_cap_bqt" name="inicio_cap_bqt" value="" placeholder="Ingrese el numero capacitacion" class="form-control" required>
          <br>
          <b>FIN:</b>
          <br>
          <input type="date" id="fin_cap_bqt" name="fin_cap_bqt" value="" placeholder="Ingrese el Email del  capacitacion" class="form-control" required>
          <br>
          <b>DEPARTAMENTO:</b>
          <br>
          <select class="form-control selectpicker" name="fk_id_dep_bqt" id="fk_id_dep_bqt" required data-live-search='true'>
            <option value=""> -- Selecione un departamento -- </option>
            <?php if ($listadoDepartamentos): ?>
              <?php foreach ($listadoDepartamentos->result() as $departamento): ?>
                <option value="<?php echo $departamento->id_dep_bqt; ?>">
                  <?php echo $departamento->nombre_dep_bqt; ?>
                  |
                  <?php echo $departamento->telefono_dep_bqt; ?> (telefono)
                </option>

              <?php endforeach; ?>

            <?php else: ?>

            <?php endif; ?>
          </select>
          <br>
          <b>OBSERVACIONES:</b>
          <br>
          <textarea name="observaciones_cap_bqt" class="form-control" rows="5" cols="80"></textarea>
          <br>
          <button   type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok" ></i>  GUARDAR</button>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
$("#tbl_capacitacion").DataTable();
</script>

<script type="text/javascript">
  $("#frm_capacitacion").validate({
    rules:{
      nombre_dep_bqt:{
        required:true,
        minlength:4
      },
      telefono_dep_bqt:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      email_dep_bqt:{
        required:true,
        email:true
      }

    },
    messages:{
        nombre_dep_bqt:{
        required:"Ingrese su nombre",
        minlength:"Nombre incorrecto"
      },
      telefono_dep_bqt:{
        required:"Ingrese su telefono",
        minlength:"Ingrese 10 campos",
        maxlength:"Ingrese 10 campos",
        digits:"Ingrese solo numeros"
      },
      email_dep_bqt:{
        required:"Ingrese su Email",
        email:"Email incorrecto"
      }
    }
  });
  </script>
