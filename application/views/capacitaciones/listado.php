<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <legend class="text-center">
        <i class="glyphicon glyphicon-globe"></i>
        GESTION DE CAPACITACIONES
      </legend>
      <br>
      <br>
      <br>
      <div class="table-responsive">
        <?php if ($listadoCapacitaciones): ?>
          <table id="tbl_capacitacion" class="table table-striped table-bordered table-hover table-responsive">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">TEMA</th>
                <th class="text-center">INICIO CAPACITACION</th>
                <th class="text-center">FIN DE CAPACITACION</th>
                <th class="text-center">OBSERVACION</th>
                <th class="text-center">DEPARTAMENTO</th>
                <th class="text-center">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listadoCapacitaciones->result() as $capacitacion): ?>
                <tr>
                  <td class="text-center"><?php echo $capacitacion->id_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->tema_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->inicio_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->fin_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->observaciones_cap_bqt;?></td>
                  <td class="text-center"><?php echo $capacitacion->nombre_dep_bqt;?></td>

                  <td class="text-center">
                    <a href="<?php echo site_url('capacitaciones/borrar');?>/<?php echo $capacitacion->id_cap_bqt;?>" class="btn btn-danger" onclick="return confirm('DESEAS ELIMINAR'); ">
                    <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3><b>NO EXISTE CAPACITACION</b></h3>
          <!-- letra b bols de negrita -->
        <?php endif; ?>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  $("#tbl_capacitacion").DataTable();
  </script>
