<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <center>
        <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#modalNuevoDepartamento">AGREGAR</button>
      </center>
    </div>
</div>
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <legend class="text-center">
        <i class="glyphicon glyphicon-globe"></i>
        GESTION DE DEPARTAMENTOS
      </legend>
      <br>
      <br>
      <br>
      <div class="table-responsive">
        <?php if ($listadoDepartamentos): ?>
          <table id="tbl_departamento" class="table table-striped table-bordered table-hover table-responsive">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">NOMBRE</th>
                <th class="text-center">TELEFONO</th>
                <th class="text-center">E-MAIL</th>
                <th class="text-center">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listadoDepartamentos->result() as $departamento): ?>
                <tr>
                  <td class="text-center"><?php echo $departamento->id_dep_bqt;?></td>
                  <td class="text-center"><?php echo $departamento->nombre_dep_bqt;?></td>
                  <td class="text-center"><?php echo $departamento->telefono_dep_bqt;?></td>
                  <td class="text-center"><?php echo $departamento->email_dep_bqt;?></td>

                  <td class="text-center">
                    <a href="<?php echo site_url('departamentos/borrar');?>/<?php echo $departamento->id_dep_bqt;?>" class="btn btn-danger" onclick="return confirm('DESEAS ELIMINAR'); ">
                    <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3><b>NO EXISTEN DEPARATAMENTOS</b></h3>
          <!-- letra b bols de negrita -->
        <?php endif; ?>
      </div>
    </div>
  </div>

<!-- The Modal -->
<div class="modal" id="modalNuevoDepartamento">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Formulario de nuevo departamento</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm_departamento" class="" action=" <?php echo site_url('departamentos/guardarDepartamentos') ?>" method="post">
          <b>NOMBRE:</b>
          <br>
          <input type="text" id="nombre_dep_bqt" name="nombre_dep_bqt" value="" placeholder="Ingrese el nombre del departamento" class="form-control" required>
          <br>
          <b>Telefono:</b>
          <br>
          <input type="text" id="telefono_dep_bqt" name="telefono_dep_bqt" value="" placeholder="Ingrese el numero de telefono" class="form-control" required>
          <br>
          <b>EMAIL:</b>
          <br>
          <input type="text" id="email_dep_bqt" name="email_dep_bqt" value="" placeholder="Ingrese el Email del departamento" class="form-control" required>
          <br>
          <button   type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok" ></i>  GUARDAR</button>
        </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
$("#tbl_departamento").DataTable();
</script>

<script type="text/javascript">
  $("#frm_departamento").validate({
    rules:{
      nombre_dep_bqt:{
        required:true,
        minlength:4
      },
      telefono_dep_bqt:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      email_dep_bqt:{
        required:true,
        email:true
      }

    },
    messages:{
        nombre_dep_bqt:{
        required:"Ingrese su nombre",
        minlength:"Nombre incorrecto"
      },
      telefono_dep_bqt:{
        required:"Ingrese su telefono",
        minlength:"Ingrese 10 campos",
        maxlength:"Ingrese 10 campos",
        digits:"Ingrese solo numeros"
      },
      email_dep_bqt:{
        required:"Ingrese su Email",
        email:"Email incorrecto"
      }
    }
  });
  </script>
