<center>
  <h1> <b>CONFIGURACION</b> </h1>
</center>

<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
      <center>
        <h1></h1>
      </center>
      <center>
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info" 
        data-bs-toggle="modal" data-bs-target="#modalNuevoCon">AGREGAR</button>
      </center>
    </div>
</div>
<!-- Modal -->
<div id="modalNuevoCon" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>INGRESE CONFIGURACION</b></h4>
      </div>
      <div class="modal-body">
          <form id="frm_nuevo_conf" class="" action="<?php echo site_url('configuraciones/guardar'); ?>" method="post">
            <b>Nombre Empresa:</b><br>
            <input type="text" id="nombre_con_bqt" name="nombre_con_bqt" value="" placeholder="NOMBRE EMPRESA" class="form-control"> <br>
            <b>RUC:</b><br>
            <input type="number" id="ruc_con_bqt" name="ruc_con_bqt"value="" placeholder="RUC" class="form-control"> <br>
            <b>Telefono:</b><br>
            <input type="number" id="telefono_con_bqt" name="telefono_con_bqt" value="" placeholder="INGRESE TELEFONO" class="form-control"> <br>
            <b>Direccion:</b><br>
            <input type="text" id="direccion_con_bqt" name="direccion_con_bqt" value="" placeholder="INGRESE DIRECCION" class="form-control"> <br>
            <b>Reprecentante Legal:</b><br>
            <input type="text" id="representante_con_bqt" name="representante_con_bqt" value="" placeholder="INGRESE REPRESENTANTE" class="form-control"> <br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-ok"></i> Guardar
            </button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
        </div>
      </div>
  </div>
</div>

<button type="button" name="button"
onclick="consultarConf()">
  Actualizar
</button>
<div class="container">
  <div id="contenedor-listado-conf">
    <center>
      <i class="fa fa-spinner fa-6x fa-spin"></i>
      <br>Espere por favor...
    </center>
  </div>
</div>
<script type="text/javascript">
    function consultarConf(){
      $("#contenedor-listado-conf")
      .html('<center><i class="fa fa-spinner fa-6x fa-spin"></i><br>Espere por favor...</center>');
      $("#contenedor-listado-conf")
      .load("<?php echo site_url('configuraciones/listado'); ?>");
    }
    consultarConf();//cargando datos de forma automatic
</script>

<script type="text/javascript">
   $("#frm_nuevo_conf").validate({
      rules:{
        nombre_con_bqt:{
          required:true
          },
        ruc_con_bqt:{
          required: true
        },
        telefono_con_bqt:{
          required: true
        },
        direccion_con_bqt:{
          required: true
        },
        representante_con_bqt:{
          required: true
        }
      },
      messages:{
        nombre_con_bqt:{
          required:"Ingrese nombre"
          },
        ruc_con_bqt:{
          required:"Ingrese ruc"
        },
        telefono_con_bqt:{
          required:"Ingrese telefono"
        },
        direccion_con_bqt:{
          required:"ingrese direccion"
        },
        representante_con_bqt:{
          required:"Ingrese nombre"
        }
      },
      submitHandler:function(formulario){
          //Ejecutando la peticion Asincrona
          $.ajax({
            type:'post',
            url:'<?php echo site_url("configuraciones/guardar"); ?>',
            data:$(formulario).serialize(),
            success:function(data){
              // alert(data);
              var objetoRespuesta=JSON.parse(data);
              if(objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK"){
                Swal.fire(
                  'CONFIRMACIÓN', //titulo
                  objetoRespuesta.mensaje,
                  'success' //Tipo de alerta
                );
                $("#modalNuevoCon").modal("hide");
                consultarConf();//cargando datos de forma automatic
              }else{
                Swal.fire(
                  'ERROR', //titulo
                  'Error al insertar, intente nuevamente',
                  'error' //Tipo de alerta
                );
              }
            }
          });
      }
   });
</script>










<!--  dxfv -->
