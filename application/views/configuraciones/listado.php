<?php if ($configuracion): ?>
  <table class="table table-bordered
  table-striped table-hover" id="tbl-confi">
      <thead>
        <tr>
          <th>ID</th>
          <th>EMPRESA</th>
          <th>RUC</th>
          <th>TELEFONO</th>
          <th>DIRECCION</th>
          <th>REPRESENTANTE</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
          <?php foreach ($configuracion->result() as $temporal): ?>
            <tr>
              <td><?php echo $temporal->id_con_bqt; ?></td>
              <td><?php echo $temporal->nombre_con_bqt; ?></td>
              <td><?php echo $temporal->ruc_con_bqt; ?></td>
              <td><?php echo $temporal->telefono_con_bqt; ?></td>
              <td><?php echo $temporal->direccion_con_bqt; ?></td>
              <td><?php echo $temporal->representante_con_bqt; ?></td>
              <td class="text-center">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalNuevoCon">Editar</button>                                      
                <a href="<?php echo site_url('configuraciones/borrar'); ?>/<?php echo $temporal->id_con_bqt; ?>" class="btn btn-danger">
                  <i class="fa fa-trash"></i>
                  <!--Amarillo - Basurero -->
                </a>
              </td>
              <div class="container">
                <div class="modal" id="modalNuevoCon">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">ACTUALIZAR DATOS</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                        <div class="modal-body">
                          <form id="frm_nuevo_conf" class="" action="<?php echo site_url('configuraciones/procesarActualizacion'); ?>" method="post">
                            <b>Nombre Empresa:</b><br>
                            <input type="text" id="nombre_con_bqt" name="nombre_con_bqt" value="" placeholder="NOMBRE EMPRESA" class="form-control"> <br>
                            <b>RUC:</b><br>
                            <input type="number" id="ruc_con_bqt" name="ruc_con_bqt"value="" placeholder="RUC" class="form-control"> <br>
                            <b>Telefono:</b><br>
                            <input type="number" id="telefono_con_bqt" name="telefono_con_bqt" value="" placeholder="INGRESE TELEFONO" class="form-control"> <br>
                            <b>Direccion:</b><br>
                            <input type="text" id="direccion_con_bqt" name="direccion_con_bqt" value="" placeholder="INGRESE DIRECCION" class="form-control"> <br>
                            <b>Reprecentante Legal:</b><br>
                            <input type="text" id="representante_con_bqt" name="representante_con_bqt" value="" placeholder="INGRESE REPRESENTANTE" class="form-control"> <br>
                            <button type="submit" name="button" class="btn btn-success">
                              <i class="glyphicon glyphicon-ok"></i> Guardar
                            </button>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                  </div>
                </div>
              
            </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
      No se ninguna configuracion.
  </div>
<?php endif; ?>
