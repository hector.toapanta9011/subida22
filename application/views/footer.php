

  <!-- JavaScript Libraries -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/chart/chart.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/tempusdominus/js/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/tempusdominus/js/moment-timezone.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

  <!-- Template Javascript -->
  <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
  <style>

      label.error{
          border:1px solid white;
          color:white;
          background-color:#E15B69;
          padding:5px;
          padding-left:15px;
          padding-right:15px;
          font-size:12px;
          opacity: 0;
            /visibility: hidden;/
            /position: absolute;/
            left: 0px;
            transform: translate(0, 10px);
            /background-color: white;/
            /padding: 1.5rem;/
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
            width: auto;
            margin-top:30px !important;


            z-index: 10;
            opacity: 1;
            visibility: visible;
            transform: translate(0, -20px);
            transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
            border-radius:10px;
            width:100%;

      }

      input.error{
          border:1px solid #E15B69;
      }


      select.error{
          border:1px solid #E15B69;
      }

      label.error:before{
           position: absolute;
            z-index: -1;
            content: "";
            right: calc(90% - 10px);
            top: -8px;
            border-style: solid;
            border-width: 0 10px 10px 10px;
            border-color: transparent transparent #E15B69 transparent;
            transition-duration: 0.3s;
            transition-property: transform;
      }


  </style>


  
  </body>



</html>


<!-- SweetAlert2 -->
<!-- // Swal.fire({
//   title: '¿Seguro que desea eliminar?',
//   showDenyButton: false,
//   showCancelButton: true,
//   confirmButtonText:'Si',
//   denyButtonText: `No`,
// }).then((result) => {
//   /* Read more about isConfirmed, isDenied below */
//   $.ajax({
//     url:"<?php echo site_url('sucursal/eliminarUsuario'); ?>",
//     type:"post",
//     data:{"id_suc_bqt":id_suc_bqt},
//     success:function(data){
//       consultarSucursales();
//       var objetoRespuesta=JSON.parse(data);
//       if (objetoRespuesta.estado=='ok'){
//         Swal.fire(
//           'confirmacion!', //Titulo
//           objetoRespuesta.mensaje, //Contenido o mensaje
//           'success'//tipo de alerta
//         );
//         $('#nuevoSucursal').modal('hide');
//         consultarSucursales();
//       } else {
//         Swal.fire(
//           'error!', //Titulo
//           'Error al insertar, intente nuevamente', //Contenido o mensaje
//           'error'//tipo de alerta
//         )
//       }
//
//     }
//   });
// }) -->
