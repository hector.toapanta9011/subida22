<div class="row">
  <div class="col-md-12 text-center well">
      <h3>ACTUALIZAR CONFIGURACION</h3>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-12">
    <?php if ($Editar): ?>
        <!-- <?php print_r($Editar); ?> -->
        <form class="container text-center" action="<?php echo site_url('Configuraciones/procesarActualizacion'); ?>" method="post">
            <center>
              <input type="hidden" name="id_con_bqt"
              value="<?php echo $Editar->id_con_bqt; ?>">
            </center>
            <div class="row">
              <div class="col-md-2 text-right">
                <label for="">EMPRESA:</label>
              </div>
              <div class="col-md-7">
                <input type="text" name="nombre_con_bqt"
                value="<?php echo $Editar->nombre_con_bqt; ?>"
                class="form-control"
                placeholder="INGRESE EMPRESA" required>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-2 text-right">
                <label for="">RUC:</label>
              </div>
              <div class="col-md-7">
                <input type="number" name="ruc_con_bqt"
                value="<?php echo $Editar->ruc_con_bqt; ?>"
                class="form-control"
                placeholder="INGRESE RUC" required>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-2 text-right">
                <label for="">TELEFONO:</label>
              </div>
              <div class="col-md-7">
                <input type="number" name="telefono_con_bqt"
                value="<?php echo $Editar->telefono_con_bqt; ?>"
                class="form-control"
                placeholder="INGRESE TELEFONO" required>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-2 text-right">
                <label for="">DIRECCION:</label>
              </div>
              <div class="col-md-7">
                <input type="text" name="direccion_con_bqt" value="<?php echo $Editar->direccion_con_bqt; ?>"
                class="form-control"
                placeholder="INGRESE TELEFONO">
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-2 text-right">
                <label for="">REPRECENTANTE:</label>
              </div>
              <div class="col-md-7">
                <input type="text" name="representante_con_bqt" value="<?php echo $Editar->representante_con_bqt; ?>"
                class="form-control"
                placeholder="INGRESE REPRECENTANTE">
              </div>
            </div>
            <br>
            
            <br>
            <div class="row">
              <div class="col-md-2">
              </div>
              <div class="col-md-7">
                  <button type="submit" name="button"
                          class="btn btn-warning">
                     <i class="glyphicon glyphicon-ok"></i>
                     Actualizar
                  </button>
                  <a href="<?php echo site_url('configuraciones/index'); ?>"
                    class="btn btn-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    Cancelar
                  </a>
              </div>
            </div>
        </form>

    <?php else: ?>
        <div class="alert alert-danger">
            <b>No se encontro configuraciones:(</b>
        </div>
    <?php endif; ?>
  </div>
</div>
