<?php
  class Capacitacion extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //Funcion de insercion
    public function insertar($datos){
      return $this->db->insert('capacitaciones',$datos);
    }

    //Funcion de mostrar todos los datos
    public function obtenerTodos(){
      $capacitaciones=$this->db->get('capacitaciones');
      if ($capacitaciones->num_rows()>0){
        return $capacitaciones;
      }
      else{
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where('id_cap_bqt',$id);
      return $this->db->delete('capacitaciones');
    }

    public function obtenerPorId($id){
      $this->db->where('id_cap_bqt',$id);
      $capacitaciones=$this->db->get('capacitaciones');
      if ($capacitaciones->num_rows()>0) {
        return $capacitaciones->row(); //porque solo existe uno
      } else {
        return false;
      }

    }

    //proceso de actualizacion de personal
    public function actualizar($id,$datos){
      $this->db->where('id_cap_bqt',$id);
      return $this->db->update('capacitaciones',$datos);
    }
  }//Cierre de la clase no borrar
