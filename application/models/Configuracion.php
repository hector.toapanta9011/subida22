<?php
   class Configuracion extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("configuraciones",$datos);
     }
     //Funcion que consulta todos los profesores de la bdd
     public function obtenerTodos(){
        $configuraciones=$this->db->get("configuraciones");
        if ($configuraciones->num_rows()>0) {
          return $configuraciones;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un profesor se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_con_bqt",$id);
        return $this->db->delete("configuraciones");
     }
     //Consultando el profesor por su id
     public function obtenerPorId($id){
        $this->db->where("id_con_bqt",$id);
        $profesor=$this->db->get("configuraciones");
        if($profesor->num_rows()>0){
          return $profesor->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de profesores
     public function actualizar($id,$datos){
       $this->db->where("id_con_bqt",$id);
       return $this->db->update("configuraciones",$datos);
    }

  }//Cierre de la clase (No borrar)
