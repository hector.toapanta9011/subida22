<?php
   class Solicitud extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("solicitudes",$datos);
     }
     //Funcion que consulta todos solicitudes de la bdd
     public function obtenerTodos(){
        $solicitudes=$this->db->get("solicitudes");
        if ($solicitudes->num_rows()>0) {
          return $solicitudes;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un solicitud se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_sol_bqt",$id);
        return $this->db->delete("solicitudes");
     }
     //Consultando solicitud por su id
     public function obtenerPorId($id){
        $this->db->where("id_sol_bqt",$id);
        $solicitudes=$this->db->get("solicitudes");
        if($solicitudes->num_rows()>0){
          return $solicitudes->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de solicitud
     public function actualizar($id,$datos){
       $this->db->where("id_sol_bqt",$id);
       return $this->db->update("solicitudes",$datos);
    }

  }//Cierre de la clase (No borrar)
