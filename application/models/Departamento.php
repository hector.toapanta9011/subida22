<?php
  class Departamento extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //Funcion de insercion
    public function insertar($datos){
      return $this->db->insert('departamentos',$datos);
    }

    //Funcion de mostrar todos los datos
    public function obtenerTodos(){
      $departamentos=$this->db->get('departamentos');
      if ($departamentos->num_rows()>0){
        return $departamentos;
      }
      else{
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where('id_dep_bqt',$id);
      return $this->db->delete('departamentos');
    }

    public function obtenerPorId($id){
      $this->db->where('id_dep_bqt',$id);
      $departamentos=$this->db->get('departamentos');
      if ($departamentos->num_rows()>0) {
        return $departamentos->row(); //porque solo existe uno
      } else {
        return false;
      }

    }

    //proceso de actualizacion de personal
    public function actualizar($id,$datos){
      $this->db->where('id_dep_bqt',$id);
      return $this->db->update('departamentos',$datos);
    }
  }//Cierre de la clase no borrar
