<?php
  class Sucursal extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }
    public function insertar($datos)
    {
      return $this->db->insert('sucursales',$datos);
    }
    //Funsion de consulta de todos los estudiantes
    public function obtenerTodos(){
      $sucursales=$this->db->get('sucursales');
      if ($sucursales->num_rows()>0) {
        return $sucursales;
      } else {
        return false; //cuando no hay datos
      }

    }
    //Funcion para eliminar un estudiante se recbe el id_est
    public function eliminarPorId($id){
      $this->db->where('id_suc_bqt',$id);
      return $this->db->delete('sucursales');
    }

    public function obtenerPorId($id){
      $this->db->where('id_suc_bqt',$id);
      $sucursales=$this->db->get('sucursales');
      if ($sucursales->num_rows()>0) {
        return $sucursales->row(); //porque solo existe uno
      } else {
        return false;
      }

    }

    //Proceso de actualizacion de estudiante
    public function actualizar($id,$datos){
      $this->db->where('id_suc_bqt',$id);
      return $this->db->update('sucursales',$datos);
    }
  } //Cierre de la clase (No borrar)
