<?php
  class Personal extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }
    public function insertar($datos)
    {
      return $this->db->insert('personales',$datos);
    }
    //Funsion de consulta de todos el personal
    public function obtenerTodos(){
      $personales=$this->db->get('personales');
      if ($personales->num_rows()>0) {
        return $personales;
      } else {
        return false; //cuando no hay datos
      }

    }
    //Funcion para eliminar un estudiante se recbe el id_pro_bqj
    public function eliminarPorId($id){
      $this->db->where('id_per_bqt',$id);
      return $this->db->delete('personales');
    }

    public function obtenerPorId($id){
      $this->db->where('id_per_bqt',$id);
      $personales=$this->db->get('personales');
      if ($personales->num_rows()>0) {
        return $personales->row(); //porque solo existe uno
      } else {
        return false;
      }

    }

    //proceso de actualizacion de personal
    public function actualizar($id,$datos){
      $this->db->where('id_per_bqt',$id);
      return $this->db->update('personales',$datos);
    }
   //Cierre de la clase (No borrar)
    //Funcion para eliminar un estudiante se recbe el id_per_bqj
    
  } //Cierre de la clase (No borrar)
